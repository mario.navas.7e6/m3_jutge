import java.sql.*

interface ProblemDAO {
    fun create(problem: Problem): Boolean
    fun read(idProblem: Int): Problem?
    fun readAll():Problems?
    fun update(problem: Problem): Boolean
    fun delete(idProblem: Int): Boolean
}

class ProblemDAOJDBC: ProblemDAO {
    private val url = "jdbc:postgresql://localhost:5432/jutge"
    private val user = "postgres"
    private val password = "savanoiram2004"

    override fun create(problem: Problem): Boolean {
        val conn = DriverManager.getConnection(url, user, password)
        val problemStmt =
            conn.prepareStatement("INSERT INTO problema (id_problema, titol, enunciat, resolt, intents) VALUES (?, ?, ?, ?, ?)")

//        val gson = Gson()
//        val jocProvesPublicJson: String = gson.toJson(problem.jProvesPublic)
        problemStmt.setInt(1, problem.idProblem)
        problemStmt.setString(2, problem.titol)
        problemStmt.setString(3, problem.enunciat)
        problemStmt.setBoolean(4, problem.resolt)
        problemStmt.setInt(5, problem.intents)

        var result = problemStmt.executeUpdate() > 0
        problemStmt.close()

        for(public in problem.jProvesPublic){
            val jpPublicstmt =
                conn.prepareStatement("INSERT INTO joc_proves_public (id_problema, inputString, outputString) VALUES (?,?,?)")

            jpPublicstmt.setInt(1, problem.idProblem)
            jpPublicstmt.setString(2, public.input)
            jpPublicstmt.setString(3,public.output)

            if(result){
                result =(jpPublicstmt.executeUpdate()<0)
            }
            jpPublicstmt.close()
        }

        for(privat in problem.jProvesPrivat){
            val jpPrivatStmt =
                conn.prepareStatement("INSERT INTO joc_proves_privat (id_problema, inputString, outputString) VALUES (?,?,?)")

            jpPrivatStmt.setInt(1, problem.idProblem)
            jpPrivatStmt.setString(2, privat.input)
            jpPrivatStmt.setString(3,privat.output)

            if(result){
                result =(jpPrivatStmt.executeUpdate()<0)
            }
            jpPrivatStmt.close()
        }

        for(intent in problem.listIntents){
            val intentStmt =
                conn.prepareStatement("INSERT INTO intents (intentResult, answer) VALUES (?,?,?)")

            intentStmt.setBoolean(2, intent.result)
            intentStmt.setString(3,intent.answer)

            if(result){
                result =(intentStmt.executeUpdate()<0)
            }
            intentStmt.close()
        }

        conn.close()


        return result
    }
    override fun read(idProblem: Int): Problem? {
        val conn = DriverManager.getConnection(url, user, password)
        var problem: Problem? = null

        try {
            val problemStmt = conn.prepareStatement("SELECT * FROM problema WHERE id_problema = ?")
            problemStmt.setInt(1, idProblem)

            val problemRs = problemStmt.executeQuery()

            if (problemRs.next()) {
                val titol = problemRs.getString("titol")
                val enunciat = problemRs.getString("enunciat")
                val resolt = problemRs.getBoolean("resolt")
                val intents = problemRs.getInt("intents")

                val jProvesPublic = mutableListOf<JocProves>()
                val jpPublicStmt = conn.prepareStatement("SELECT * FROM joc_proves_public WHERE id_problema = ?")
                jpPublicStmt.setInt(1, idProblem)
                val jpPublicRs = jpPublicStmt.executeQuery()

                while (jpPublicRs.next()) {
                    val input = jpPublicRs.getString("inputString")
                    val output = jpPublicRs.getString("outputString")
                    jProvesPublic.add(JocProves(input, output))
                }
                jpPublicStmt.close()


                val jProvesPrivat = mutableListOf<JocProves>()
                val jpPrivatStmt = conn.prepareStatement("SELECT * FROM joc_proves_privat WHERE id_problema = ?")
                jpPrivatStmt.setInt(1, idProblem)
                val jpPrivatRs = jpPrivatStmt.executeQuery()

                while (jpPrivatRs.next()) {
                    val input = jpPrivatRs.getString("inputString")
                    val output = jpPrivatRs.getString("outputString")
                    jProvesPrivat.add(JocProves(input, output))
                }
                jpPrivatStmt.close()


                val listIntents = mutableListOf<Intent>()
                val intentStmt = conn.prepareStatement("SELECT * FROM intents WHERE id_problema = ?")
                intentStmt.setInt(1, idProblem)
                val intentRs = intentStmt.executeQuery()

                while (intentRs.next()) {
                    val result = intentRs.getString("intentResult")
                    val answer = intentRs.getString("answer")
                    listIntents.add(Intent(result.toBoolean(), answer))
                }
                intentStmt.close()

                problem = Problem(idProblem, titol, enunciat, jProvesPublic, jProvesPrivat, resolt, intents, listIntents)
            }

            problemRs.close()
            problemStmt.close()

        } catch (e: SQLException) {
            e.printStackTrace()
        } finally {
            conn.close()
        }

        return problem
    }
//    override fun read(problemId: Int): Problem? {
//        var problem: Problem? = null
//
//        val conn = DriverManager.getConnection(url, user, password)
//        val problemStmt = conn.prepareStatement("SELECT * FROM problema WHERE id_problema = ?")
//        problemStmt.setInt(1, problemId)
//        val problemResultSet = problemStmt.executeQuery()
//
//        if (problemResultSet.next()) {
//            val idProblem = problemResultSet.getInt("id_problema")
//            val titol = problemResultSet.getString("titol")
//            val enunciat = problemResultSet.getString("enunciat")
//            val resolt = problemResultSet.getBoolean("resolt")
//            val intents = problemResultSet.getInt("intents")
//
//            val jProvesPublicStmt =
//                conn.prepareStatement("SELECT * FROM joc_proves_public WHERE id_problema = ?")
//            jProvesPublicStmt.setInt(1, problemId)
//            val jProvesPublicResultSet = jProvesPublicStmt.executeQuery()
//
//            val jProvesPublicList = mutableListOf<JocProves>()
//
//            while (jProvesPublicResultSet.next()) {
//                val input = jProvesPublicResultSet.getString("inputString")
//                val output = jProvesPublicResultSet.getString("outputString")
//                val jocProva = JocProves(input, output)
//                jProvesPublicList.add(jocProva)
//            }
//            jProvesPublicStmt.close()
//
//            val jProvesPrivatStmt =
//                conn.prepareStatement("SELECT * FROM joc_proves_privat WHERE id_problema = ?")
//            jProvesPrivatStmt.setInt(1, problemId)
//            val jProvesPrivatResultSet = jProvesPrivatStmt.executeQuery()
//
//            val jProvesPrivatList = mutableListOf<JocProves>()
//
//            while (jProvesPrivatResultSet.next()) {
//                val input = jProvesPrivatResultSet.getString("inputString")
//                val output = jProvesPrivatResultSet.getString("outputString")
//                val jocProva = JocProves(input, output)
//                jProvesPrivatList.add(jocProva)
//            }
//            jProvesPrivatStmt.close()
//
//            val listIntentsStmt = conn.prepareStatement("SELECT * FROM intents WHERE id_problema = ?")
//            listIntentsStmt.setInt(1, problemId)
//            val listIntentsResultSet = listIntentsStmt.executeQuery()
//
//            val listIntents = mutableListOf<Intent>()
//
//            while (listIntentsResultSet.next()) {
//                val result = listIntentsResultSet.getString("intentResult")
//                val answer = listIntentsResultSet.getString("answer")
//                val intent = Intent(result.toBoolean(), answer)
//                listIntents.add(intent)
//            }
//            listIntentsStmt.close()
//
//            problem = Problem(idProblem, titol, enunciat, resolt, intents, jProvesPublicList, jProvesPrivatList, listIntents)
//        }
//
//        problemStmt.close()
//
//        conn.close()
//
//        return problem
//    }

//    override fun read(idProblem: Int): Problem? {
//        val conn = DriverManager.getConnection(url, user, password)
//        val stmt = conn.prepareStatement("SELECT * FROM problems WHERE idProblem = ?")
//
//        stmt.setInt(1, idProblem)
//
//        val resultSet = stmt.executeQuery()
//        val problem: Problem? = if (resultSet.next()) {
//            Problem(
//                resultSet.getInt("idProblem"),
//                resultSet.getString("titol"),
//                resultSet.getString("enunciat"),
//                resultSet.getArray("jProvesPublic").array as MutableList<JocProves>,
//                resultSet.getArray("jProvesPrivat").array as MutableList<JocProves>,
//                resultSet.getBoolean("resolt"),
//                resultSet.getInt("intents"),
//                resultSet.getArray("listIntents").array as MutableList<Intent>
//            )
//        } else {
//            null
//        }
//
//        resultSet.close()
//        stmt.close()
//        conn.close()
//
//        return problem
//    }

    override fun readAll(): Problems? {
        val conn = DriverManager.getConnection(url, user, password)
        val selectStmt = conn.prepareStatement("SELECT * FROM problema")
        val resultSet = selectStmt.executeQuery()

        val problems = Problems(mutableListOf())

        while (resultSet.next()) {
            val idProblem = resultSet.getInt("id_problema")
            val titol = resultSet.getString("titol")
            val enunciat = resultSet.getString("enunciat")
            val resolt = resultSet.getBoolean("resolt")
            val intents = resultSet.getInt("intents")

            // Retrieve joc_proves_public for the problem
            val jpPublicSelectStmt = conn.prepareStatement("SELECT * FROM joc_proves_public WHERE id_problema = ?")
            jpPublicSelectStmt.setInt(1, idProblem)
            val jpPublicResultSet = jpPublicSelectStmt.executeQuery()
            val jProvesPublic = mutableListOf<JocProves>()

            while (jpPublicResultSet.next()) {
                val input = jpPublicResultSet.getString("inputString")
                val output = jpPublicResultSet.getString("outputString")
                val jp = JocProves(input, output)
                jProvesPublic.add(jp)
            }
            jpPublicResultSet.close()
            jpPublicSelectStmt.close()

            // Retrieve joc_proves_privat for the problem
            val jpPrivatSelectStmt = conn.prepareStatement("SELECT * FROM joc_proves_privat WHERE id_problema = ?")
            jpPrivatSelectStmt.setInt(1, idProblem)
            val jpPrivatResultSet = jpPrivatSelectStmt.executeQuery()
            val jProvesPrivat = mutableListOf<JocProves>()

            while (jpPrivatResultSet.next()) {
                val input = jpPrivatResultSet.getString("inputString")
                val output = jpPrivatResultSet.getString("outputString")
                val jp = JocProves(input, output)
                jProvesPrivat.add(jp)
            }
            jpPrivatResultSet.close()
            jpPrivatSelectStmt.close()

            // Retrieve intents for the problem
            val intentSelectStmt = conn.prepareStatement("SELECT * FROM intents WHERE id_problema = ?")
            intentSelectStmt.setInt(1, idProblem)
            val intentResultSet = intentSelectStmt.executeQuery()
            val listIntents = mutableListOf<Intent>()

            while (intentResultSet.next()) {
                val result = intentResultSet.getBoolean("intentResult")
                val answer = intentResultSet.getString("answer")
                val intent = Intent(result, answer)
                listIntents.add(intent)
            }
            intentResultSet.close()
            intentSelectStmt.close()

            val problem = Problem(idProblem, titol, enunciat, jProvesPublic, jProvesPrivat, resolt, intents, listIntents)
            problems.list.add(problem)
        }

        resultSet.close()
        selectStmt.close()
        conn.close()

        return problems
    }

    override fun update(problem: Problem): Boolean {
        val conn = DriverManager.getConnection(url, user, password)
        val problemStmt =
            conn.prepareStatement("UPDATE problema SET titol = ?, enunciat = ?, resolt = ?, intents = ? WHERE id_problema = ?")

        problemStmt.setString(1, problem.titol)
        problemStmt.setString(2, problem.enunciat)
        problemStmt.setBoolean(3, problem.resolt)
        problemStmt.setInt(4, problem.intents)
        problemStmt.setInt(5, problem.idProblem)

        var result = problemStmt.executeUpdate() > 0
        problemStmt.close()

        // Update joc_proves_public table
        val jpPublicDeleteStmt =
            conn.prepareStatement("DELETE FROM joc_proves_public WHERE id_problema = ?")
        jpPublicDeleteStmt.setInt(1, problem.idProblem)
        jpPublicDeleteStmt.executeUpdate()
        jpPublicDeleteStmt.close()

        for (public in problem.jProvesPublic) {
            val jpPublicstmt =
                conn.prepareStatement("INSERT INTO joc_proves_public (id_problema, inputString, outputString) VALUES (?,?,?)")

            jpPublicstmt.setInt(1, problem.idProblem)
            jpPublicstmt.setString(2, public.input)
            jpPublicstmt.setString(3, public.output)

            if (result) {
                result = (jpPublicstmt.executeUpdate() < 0)
            }
            jpPublicstmt.close()
        }

        // Update joc_proves_privat table
        val jpPrivatDeleteStmt =
            conn.prepareStatement("DELETE FROM joc_proves_privat WHERE id_problema = ?")
        jpPrivatDeleteStmt.setInt(1, problem.idProblem)
        jpPrivatDeleteStmt.executeUpdate()
        jpPrivatDeleteStmt.close()

        for (privat in problem.jProvesPrivat) {
            val jpPrivatStmt =
                conn.prepareStatement("INSERT INTO joc_proves_privat (id_problema, inputString, outputString) VALUES (?,?,?)")

            jpPrivatStmt.setInt(1, problem.idProblem)
            jpPrivatStmt.setString(2, privat.input)
            jpPrivatStmt.setString(3, privat.output)

            if (result) {
                result = (jpPrivatStmt.executeUpdate() < 0)
            }
            jpPrivatStmt.close()
        }
        // Update intents table
        val intentDeleteStmt =
            conn.prepareStatement("DELETE FROM intents WHERE id_problema = ?")
        intentDeleteStmt.setInt(1, problem.idProblem)
        intentDeleteStmt.executeUpdate()
        intentDeleteStmt.close()

        for (intent in problem.listIntents) {
            val intentStmt =
                conn.prepareStatement("INSERT INTO intents (id_problema, intentResult, answer) VALUES (?,?,?)")

            intentStmt.setInt(1, problem.idProblem)
            intentStmt.setBoolean(2, intent.result)
            intentStmt.setString(3, intent.answer)

            if (result) {
                result = (intentStmt.executeUpdate() < 0)
            }
            intentStmt.close()
        }

        conn.close()

        return result
    }




     override fun delete(idProblem: Int): Boolean {
        val conn = DriverManager.getConnection(url, user, password)
        val deleteStmt = conn.prepareStatement("DELETE FROM problems WHERE idProblem = ?")

        deleteStmt.setInt(1, idProblem)

        val result = deleteStmt.executeUpdate() > 0

        deleteStmt.close()
        conn.close()

        return result
    }

}