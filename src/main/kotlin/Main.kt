
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.appendText
import kotlin.io.path.writeText
import kotlin.system.exitProcess
val dao: ProblemDAO = ProblemDAOJDBC()
val scanner = Scanner(System.`in`)
data class Problems(
    var list: MutableList<Problem>
)
class Robot {
    private fun exit(){
        exitProcess(-1)
    }
   private fun help(){
       println("La opción continuar história te permite jugar el primer problema que no hayas resuelto.\n" +
               "La opción elegir problema te enseña todos los problemas que hay en el juego y te permite elegir cualquiera al que jugar.\n" +
               "La opción Historial de problemas te enseña todos los intentos que has hecho de cada problema y si lo has resuelto o no.\n" +
               "La opción Salir, cierra el programa.\n")
       this.menu()
   }
   private fun chooseProblem(){
       println("Lista con todos los problemas del juego:")
       for(i in 0..problems.list.lastIndex){
           println("${i}.${problems.list[i].titol}")
       }
       println("Introduce el numero de el que quieras jugar?:")
       val number = scanner.nextInt()
       problems.list[number].problem()
   }
   private fun problemsHistory(){
        for(i in problems.list){
            if(i.resolt){
                println("${i.titol}(RESUELTO)")
            }else{
                println("${i.titol}(NO RESUELTO)")
            }
            for(j in i.listIntents){
                if(!j.result){
                    println("\u001b[31m -Respuesta:${j.answer} \u001b[39m")
                }else{
                    println("\u001b[32m -Respuesta:${j.answer} \u001b[39m")
                }
            }
            println()
        }
       will(0, Problem(problems.list.size,"",""))
   }

   private fun continueGame(){
       if(!introduction){
           for(i in story){
               println(i)
               pressEnter()
           }
           introduction = true
       }
       for( i in 0..problems.list.lastIndex){
           if(!problems.list[i].resolt){
               problems.list[i].problem()
           }
       }
   }
    fun will(answer :Int, problema: Problem){
        var will: Int
        do {
            when(answer){
                0-> println("Si quieres salir al menu (2):")
                1-> println("UPS! Fallaste\nQuieres volverlo a intentar o salir al menu? \nIntroduce 1(RESOLVER) o 2(SALIR):")
                2-> println("BIEEN! Lo hiciste!\n Quieres continuar con el siguiente nivel, repetir este o salir al menu? \nIntroduce 0(siguiente) 1(REPETIR) o 2(SALIR):")
            }

            will = scanner.nextInt()

        } while (will != 1 && will != 2 && will !=0)
            when(will){
                1-> problema.problem()
                2->menu()
            }
    }
    fun menu(){
        var option: Int
        do{
            println("Hi, itbot, que quieres hacer?\n" +
                    "1.Continuar Historia\n" +
                    "2.Elegir Problema\n" +
                    "3.Historial de Problemas\n" +
                    "4.Ayuda\n" +
                    "5.Salir")
            println("Que quieres hacer: ")
            option = scanner.nextInt()
        } while (option !in 1..5)

        when(option){
            1->this.continueGame()
            2->this.chooseProblem()
            3->this.problemsHistory()
            4->this.help()
            5->this.exit()
        }
    }
}
class Developer{
    private fun createProblem(){
        println("Introduce el nuevo título:")
        val newTitle = scanner.nextLine()
        println("Introduce el nuevo enunciado:")
        val newStatement = scanner.next()
        problems.list.add(Problem(problems.list.size, newTitle, newStatement))
        println("Ahora intoduce los juegos de pruevas como mínimo uno privado y uno público." +
                "Introduce los Juegos de Pruevas Púbicos." +
                "Cantidad de JdP que desea introducir:")
        var quantity = scanner.nextInt()
        repeat(quantity){
            println("Introduce la entrada en una linea y la salida en la siguiente linea:")
            val entrada = scanner.nextLine()
            val sortida = scanner.nextLine()
            val problem = problems.list[problems.list.lastIndex]
//            val numOfJPoves = problem.jProvesPrivat.size+problem.jProvesPublic.size
            problem.addJPPublic(JocProves(entrada, sortida))
            println("Juego de pruebas añadido")
        }
        println("Ahora los Juegos de Pruevas Privados")
        quantity = scanner.nextInt()
        repeat(quantity){
            println("Introduce la entrada en una linea y la salida en la siguiente linea:")
            val entrada = scanner.nextLine()
            val sortida = scanner.nextLine()
            val problem = problems.list[problems.list.lastIndex]
//            val numOfJPoves = problem.jProvesPrivat.size+problem.jProvesPublic.size

            problem.addJPPrivate(JocProves( entrada, sortida))
            println("Juego de pruebas añadido")
        }
        dao.create(problems.list[problems.list.lastIndex])

//        val problemsJSON = Path("./src/main/kotlin/problems.json")
//        problemsJSON.writeText("")
//        for(i in problems.list){
//            problemsJSON.appendText(Json.encodeToString(i) + "\n")
//        }
        menu()
    }

    private fun robotsHistory(){
        var punctuation = 0
        var numberProblems = 0
        for(i in problems.list){
            if(i.resolt){
                println("${i.titol}(RESUELTO)")
            }else{
                println("${i.titol}(NO RESUELTO)")
            }
            var tries = 0
            for(j in i.listIntents){
                if(!j.result){
                    println("\u001b[31m -Respuesta:${j.answer} \u001b[39m")
                }else{
                    println("\u001b[32m -Respuesta:${j.answer} \u001b[39m")
                }
                tries++
            }
            numberProblems++
            punctuation += 10-tries
            println()
        }
        val relativePuncuation: Double = punctuation/numberProblems.toDouble()
        println("La puntuación relativa del robot es de $relativePuncuation")
        println()
        menu()

    }
    fun menu(){
        var option: Int
        do{
            println("Hola, developer, tienes las siguientes opciones:\n" +
                    "   1.Crear Problema\n" +
                    "   2.Historial del Robot" +
                    "" +
                    "" +
                    "" )
            println("Que quieres hacer: ")
            option = scanner.nextInt()
        } while (option !in 1..2)

        when(option){
            1->this.createProblem()
            2->this.robotsHistory()
        }
    }
}

var story = listOf("Hola RFTY5220QHW, Bienvenido a ITBOT, tu eres su nuevo procesador, de ultima generacion, especializado en robots con IA,\n" +
        "ITBOT es un robot multitarea, destinado primncipalmente a ayudar su propietario en las labores del hogar, pero capacitado para acompañar en el ocio, enseñar, llevar la renta etc.\n" +
        "Tu seras el encargado de llevar a cabo las operaciones mas complejas y juntos ireis aprendiendo y ganando habilidad y velocidad.\n" +
        "No tenemos demasiada información sobre vuestra pasada vida ni sobre vuestro dueño. Ya que el cambio de procesador a supuesto la perdida de los datos\n" +
        "Ademas es impossible encontrar información de vuestro dueño en la red. Todo lo que sabemos es por los paramentro e información que el nos ha dado:\n",
        "Se trata de un baron anciano de 82 años, estatura de 1'75m y peso de 85kg aproximadamente. Tiene la pierna derecha 15cm mas larga que la izquierda y por eso siempre va con una bota\n " +
        "con alargación\n" +
        "Se llama Manolo, pero se hace llamar Don Lolo, viudo desde hace cinco años, en su personalidad se describe como: No me toques mucho los cojones. Lo qual me parece extraño.\n " +
        "Eso es todo, te deseo mucha suerte, por cierto yo soy tu base de datos, te iré explicando todo lo que necesites saber estaremos en contacto\n "

        , "Buenos Dias, ahora mismo nos encontramos en el Super Mercado de debajo de tu casa, Don Lolo te ha mandado hacer la compra. Tendras que ir ayudando a ITBOT a lo que necesite. Suerte!"
)

var problems : Problems = Problems(mutableListOf())
var introduction = false

fun main() {
//    while(){
//        problems.list.add(dao.read()!!)
//    }
    problems = dao.readAll()!!

    var rol: Int

    do{
        println(
            "   " + "\u001b[96m" + " _____________________________________\n" +
                    "   ____  _/__  __/__  __ )_  __ \\__  __/\n" +
                    "    __  / __  /  __  __  |  / / /_  /   \n" +
                    "   __/ /  _  /   _  /_/ // /_/ /_  /    \n" +
                    "   /___/  /_/    /_____/ \\____/ /_/ " + "\u001b[39m" + "      \n" +
                    "                                     \n" +
                    "            Enter as:  1.Robot  2.Developer          \n" +
                    "" +
                    "Indicate your rol: "
        )
        rol = scanner.nextInt()
    }while(rol!=1 && rol!=2)

    if(rol== 1){
        robot()
    }else{        developer()
    }




}

fun robot(){
    Robot().menu()
}
fun developer(){
    Developer().menu()
}

fun pressEnter() {
    var enter: String?
    do{
        print("Press Enter tu Continue")
        enter = scanner.nextLine()
    }while( enter != "")
    println()
}