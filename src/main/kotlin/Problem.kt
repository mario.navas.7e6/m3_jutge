import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.io.path.Path
import kotlin.io.path.appendText
import kotlin.io.path.writeText

@Serializable
class Problem(
    var idProblem: Int,
    var titol: String,
    var enunciat:String,
    var jProvesPublic: MutableList<JocProves> = mutableListOf(),
    var jProvesPrivat: MutableList<JocProves> = mutableListOf(),
    var resolt: Boolean = false,
    var intents: Int = 0,
    var listIntents: MutableList<Intent> = mutableListOf()

) {
    fun addJPPublic(new: JocProves) {
        jProvesPublic.add(new)
    }

    fun addJPPrivate(new: JocProves) {
        jProvesPrivat.add(new)
    }

    private fun addIntent(new: Intent) {
        listIntents.add(new)
    }

    fun problem() {
        println()
        println()
        println(enunciat)
        println()
        val jprovesPublic = jProvesPublic.random()
        println("Aquí tienes un juego de pruevas público:\n -Entrada: ${jprovesPublic.input}\n -Sortida: ${jprovesPublic.output} ")
        val actualJocPrivat = jProvesPrivat.random()
        println("Resuelve el Siguiente:\n -Entrada: ${actualJocPrivat.input}\n -Sortida: ")
        val answer = scanner.next()

        if (answer == actualJocPrivat.output) {
//            val time = LocalDateTime.now()
            addIntent(Intent(true, answer))
            this.intents ++
            this.resolt = true
            dao.update(this)
//            val problemsJSON = Path("./src/main/kotlin/problems.json")
//            problemsJSON.writeText("")
//            for(i in problems.list){
//                problemsJSON.appendText(Json.encodeToString(i) + "\n")
//            }
            Robot().will(2, this)
        } else {
//            val time = LocalDateTime.now()
            addIntent(Intent(false, answer))
            this.intents ++
            dao.update(this)
//            val problemsJSON = Path("./src/main/kotlin/problems.json")
//            problemsJSON.writeText("")
//            for(i in problems.list){
//                problemsJSON.appendText(Json.encodeToString(i) + "\n")
//            }
            Robot().will(1, this)
        }
    }
}