
import kotlinx.serialization.Serializable

@Serializable
data class JocProves(
    var input:String,
    var output:String
)