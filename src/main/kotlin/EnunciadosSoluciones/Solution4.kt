fun main(){
    val price= scanner.nextInt()
    var products: MutableList<Int> = mutableListOf()
    var newProduct = scanner.nextInt()
    var productsPrice = 0
    var productsAvg = 0
    while(newProduct!=0){
        products.add(newProduct)
        newProduct = scanner.nextInt()
    }
    for(i in products){
        productsPrice+=i
    }
    productsAvg = productsPrice/products.size
    when(productsAvg){
        in price-2..price+2-> println("El precio medio se acerca al esperado")
        in 0 until price-2 -> println("El precio medio esta por debajo del esperado")
        else -> println("El precio medio esta por encima del esperado")
    }
}