import java.util.*


fun main(){
    val scanner = Scanner(System.`in`)
    val setItems : MutableSet<String> = mutableSetOf()
    val setRepeated: MutableSet<String> = mutableSetOf()
    var none = true
    var newItem = scanner.next()

    while(newItem!="END"){
        if(setItems.contains(newItem)){
            setRepeated.add(newItem)
            none = false
        }else{
            setItems.add(newItem)
        }
        newItem = scanner.next()
    }
    if(none){
        println("No hay ningun elemento repetido en la cesta")
    }else{
        for(i in setRepeated){
            println("$i esta repetido")
        }
    }
}