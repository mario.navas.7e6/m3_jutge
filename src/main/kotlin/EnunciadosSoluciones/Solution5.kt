fun main(){
    var file = 0
    var fastest: Pair<String, Int>? = null
    var newFile = scanner.nextLine()

    do{

        var newFileList = newFile.split(" ")
        var fileAvg = 0
        for(i in newFileList){
            when(i){
                "chicles"-> fileAvg+=1
                "cena"-> fileAvg+=2
                "mes"->fileAvg+=3
                "bunker"->fileAvg+=4
            }
        }
        if(fastest==null) fastest = Pair("fila ${file+1}", fileAvg)
        else if(fileAvg < fastest.second){
            fastest= Pair("fila ${file+1}", fileAvg)
        }
        file++
        newFile = scanner.nextLine()
    }while(newFile!="END")

    println("Dirigete a la ${fastest!!.first}")
}